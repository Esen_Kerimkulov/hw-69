import React from 'react';
import ReactDOM from 'react-dom';
import {createStore,applyMiddleware, compose, combineReducers} from "redux";
import thunkMiddleware from 'redux-thunk';
import {Provider} from "react-redux";
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import dishReducer from "./store/reducers/dishReducer";
import cartReducer from "./store/reducers/cartReducer";

const rootReducer = combineReducers({
    dishes: dishReducer,
    cart: cartReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunkMiddleware)));


const app = (
    <Provider store={store}>
        <App/>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();