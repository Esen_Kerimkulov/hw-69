import React, { Component } from 'react';
import './App.css';
import DishesMenu from "./components/DishMenu/DishMenu";

import Card from "./components/Card/Card";

class App extends Component {
  render() {
    return (
        <div className="App">
          <DishesMenu/>
            <Card/>
        </div>
    );
  }
}


export default App;
