import React, {Component} from 'react';
import {addDish, getDishes} from "../../store/actions/action";
import {connect} from "react-redux";
import './DishMenu.css';

class DishMenu extends Component {
    componentDidMount() {
        this.props.getDishes()
    };

    render() {

        const dishesArray = Object.keys(this.props.dishes).map(name => {
            return {...this.props.dishes[name]}
        });
        let dishesForMap = dishesArray.map((dish, key) => {
            return (
                <div className="dishMenu" key={key}>
                    <img src={dish.img}/>
                    <h3>{dish.title}</h3>
                    <p className="price">{dish.price}KGS</p>
                    <button type="button" className="button"
                            onClick={() => this.props.addDishes({title: dish.title, price: dish.price})}>
                        Add to cart
                    </button>
                </div>
            )
        });

        return (
            <div className="dishes-block">
                <h1>Menu:</h1>
                {dishesForMap}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    dishes: state.dishes.dishes
});

const mapDispatchToProps = dispatch => ({
    getDishes: () => dispatch(getDishes()),
    addDishes: (dish) => dispatch(addDish(dish)),
});
export default connect(mapStateToProps, mapDispatchToProps)(DishMenu);