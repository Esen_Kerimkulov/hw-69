import React, {Component} from 'react';
import {connect} from "react-redux";
import './Card.css';
import {removeDish} from "../../store/actions/action";


class Card extends Component {
    render() {
        const ordersArray = Object.keys(this.props.dishes).map(order => {
            if (this.props.cart[order] < 1) {
                return null
            }


            return (
                <div className="card" key={order}>
                    <p className="card-p">{this.props.dishes[order].title}</p>
                    <p className="card-p">x {this.props.cart[order]},</p>
                    <p className="card-p">Price
                        : {this.props.dishes[order].price * this.props.cart[order]}</p>
                    <button type="button" className="delete-btn"
                            onClick={() => this.props.removeDish(this.props.dishes[order].title,
                                this.props.dishes[order].price)}>
                        {'\u2718'}
                    </button>
                </div>
            )

        });
        return (
            <div>
                <div>
                    <p style={{color: 'darkblue'}}>Доставка: {this.props.delivery}</p>
                    <h1 className="total-price">Total price: {this.props.totalPrice + this.props.delivery}</h1>
                </div>
                {ordersArray}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    dishes: state.dishes.dishes,
    cart: state.cart.cart,
    totalPrice: state.cart.totalPrice,
    delivery: state.cart.delivery,
});

const mapDispatchToProps = dispatch => ({
    removeDish: (dish, price) => dispatch(removeDish(dish, price))
});


export default connect(mapStateToProps, mapDispatchToProps)(Card);