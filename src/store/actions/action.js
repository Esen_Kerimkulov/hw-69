import axios from '../../axios-menu';
import {ADD_DISH, REMOVE_DISH, orderRequest, orderSuccess, orderFailure} from "./actionType";

export const addDish = dish => ({type: ADD_DISH, dish});

export const removeDish = (dish, price) => ({type: REMOVE_DISH, dish, price});

export const getDishes = () => {
    return dispatch=> {
        dispatch(orderRequest());
        axios.get('dishes.json').then(response => {
            dispatch(orderSuccess(response.data));
        },error => {
            dispatch(orderFailure(error))
        })
    }
};
