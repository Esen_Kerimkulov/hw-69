import {ORDER_SUCCESS} from "../actions/actionType";

const initialState = {
    dishes: {}
};

const dishReducer = (state = initialState, action) => {
    switch (action.type) {

        case ORDER_SUCCESS:
            return { ...state, dishes: action.dishes};

        default:
            return state
    }
};


export default dishReducer;