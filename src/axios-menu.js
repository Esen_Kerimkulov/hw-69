import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://menu-esen.firebaseio.com/'
});

export default instance;